import 'package:flutter/material.dart';

class Riwayat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.white,

        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          elevation: 0,
          title: Text(
            'Riwayat',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontFamily: 'NExa Bold',
              fontSize: 18,
            ),
          ),
        ),
        body: ListView(
          children: [
            ListTile(
                title: Text("Elis Sindi R"),
                subtitle: Text("iam okay..."),
                leading: CircleAvatar(),
                trailing: Text("10:00 PM")
            ),
            ListTile(
                title: Text("Granger"),
                subtitle: Text("where are u?"),
                leading: CircleAvatar(),
                trailing: Text("08:00 PM")
            ),
          ],
        ),
      ),
    );
  }
}