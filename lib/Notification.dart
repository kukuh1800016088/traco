import 'package:flutter/material.dart';

class Notif extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.white,

        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          elevation: 0,
          leading: IconButton(
                icon: Icon(Icons.arrow_back_ios,
                color: Colors.black),
                onPressed: () {
                  Navigator.of(context).pop();
                }),
          title: Text(
            'Notification',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontFamily: 'Poppins',
              fontSize: 14,
            ),
          ),
        ),
        body: ListView(
          children: [
            ListTile(
                title: Text("Elis Sindi R"),
                subtitle: Text("yeayyy pesananmu akan segera dijemput"),
                leading: CircleAvatar(),
                trailing: Text("10 : 00 PM")
            ),
          ],
        ),
      ),
    );
  }
}

