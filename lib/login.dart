import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => new _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
            padding: EdgeInsets.only(top: 88, left: 50, bottom: 40),
            child: Image.asset("images/logo.png", height: 66, width: 153)),
        Container(
          child: Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                    top: 0.0, left: 50.0, bottom: 0.0, right: 0.0),
                child: Text('Selamat',
                    style: TextStyle(
                        fontFamily: 'Nexa Bold',
                        fontWeight: FontWeight.bold,
                        fontSize: 40.0)),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: 40.0, left: 50.0, bottom: 0.0, right: 0.0),
                child: Text('Datang!',
                    style: TextStyle(
                        fontFamily: 'Nexa Bold',
                        fontWeight: FontWeight.bold,
                        fontSize: 40.0)),
              ),
            ],
          ),
        ),
        Container(
            padding: EdgeInsets.only(top: 50.0, left: 50.0, right: 50.0),
            child: Column(
              children: <Widget>[
                TextField(
                  decoration: InputDecoration(
                      labelText: 'Email',
                      labelStyle: TextStyle(
                          fontFamily: 'Nexa Light', fontSize: 14, color: Colors.grey),
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.green))),
                ),
                SizedBox(height: 20.0),
                TextField(
                  decoration: InputDecoration(
                      labelText: 'Kata Sandi',
                      labelStyle: TextStyle(
                          fontFamily: 'Nexa Light', fontSize: 14, color: Colors.grey),
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.green))),
                  obscureText: true,
                ),
                SizedBox(height: 5.0),
                Container(
                  alignment: Alignment(1.0, 0.0),
                  padding: EdgeInsets.only(top: 15.0, left: 20.0),
                  child: InkWell(
                    onTap: () {
                Navigator.of(context).pushNamed('/forgotpw');
              },
                    child: Text(
                      'Lupa kata sandi?',
                      style: TextStyle(
                          color: Colors.green,
                          fontFamily: 'Nexa',
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(height: 40.0),
                Container(
                  height: 45.0,
                  width: 212.0,
                  child: Material(
                    borderRadius: BorderRadius.circular(20.0),
                    shadowColor: Colors.greenAccent,
                    color: Colors.green,
                    elevation: 7.0,
                    child: InkWell(
                      onTap: () {
                            Navigator.of(context).pushNamed('/masuk');
                          },
                      child: Center(
                        child: Text(
                          'Masuk',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Nexa Bold',
                              fontSize: 15),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )),
        SizedBox(height: 15.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Belum mempunyai akun?',
              style: TextStyle(fontFamily: 'Nexa Light'),
            ),
            SizedBox(width: 5.0),
            InkWell(
              onTap: () {
                Navigator.of(context).pushNamed('/signup');
              },
              child: Text(
                'Daftar',
                style: TextStyle(
                    color: Colors.green,
                    fontFamily: 'Nexa Bold',
                    fontWeight: FontWeight.bold,
                    fontSize: 15),
              ),
            )
          ],
        )
      ],
    ));
  }
}
