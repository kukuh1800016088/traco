import 'package:flutter/material.dart';

class Jual extends StatelessWidget {
 
  @override
  Widget build(BuildContext context) {
    var cardTextStyle = TextStyle(fontFamily: 'Nexa Bold', fontSize: 16);
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          elevation: 0,
          leading: IconButton(
                icon: Icon(Icons.close,
                color: Colors.black),
                onPressed: () {
                  Navigator.of(context).pop();
                }),
          title: Text(
            'Pilih Kategori',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontFamily: 'Nexa Bold',
              fontSize: 18,
            ),
          ),
        ),
        body: SafeArea(
          child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Column(
                children: <Widget>[
                  Text('Pilih kategori sampah yang ingin anda jual', style: TextStyle(
                            fontFamily: 'Nexa Light', fontSize: 15
                          ),),
                          SizedBox(height: 20),
                  Expanded(
                    child: GridView.count(
                        mainAxisSpacing: 10,
                        crossAxisSpacing: 10,
                        primary: false,
                        children: <Widget>[
                          Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  '/images/botol.jpg',
                                  height: 72,
                                ),
                                Text('Botol Plastik', style: cardTextStyle)
                              ],
                            ),
                          ),
                           Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  '/images/kertas.jpg',
                                  height: 72,
                                ),
                                SizedBox(height: 10),
                                Text('Kertas', style: cardTextStyle)
                              ],
                              
                            ),
                          ),

                          Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  '/images/kaleng.jpg',
                                  height: 72,
                                ),
                                Text('Logam', style: cardTextStyle)
                              ],
                            ),
                          ),

                          Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  '/images/elektronik.jpg',
                                  height: 72,
                                ),
                                Text('Elektronik', style: cardTextStyle)
                              ],
                            ),
                          ),

                          Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  '/images/karet.jpg',
                                  height: 72,
                                ),
                                Text('Karet', style: cardTextStyle)
                              ],
                            ),
                          ),

                          Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  '/images/kaca.jpg',
                                  height: 72,
                                ),
                                Text('Kaca', style: cardTextStyle)
                              ],
                            ),
                          )
                        ],
                        crossAxisCount: 3,
                        ),
                  ),
                ],),
    ),
    )
    );
  }
}