import 'package:flutter/material.dart';

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
            leading: IconButton(
                icon: Icon(Icons.arrow_back_ios),
                onPressed: () {
                  Navigator.of(context).pop();
                }),
            iconTheme: IconThemeData(
              color: Colors.black, //change your color here
            ),
            backgroundColor: Colors.white),
        body: ListView(
           children: <
            Widget>[
              Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsets.only(top: 88, left: 50, bottom: 25),
            child: Image.asset("/images/logo.png",
            height: 66,
            width: 153)),
           Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 0.0, left: 50.0, bottom: 0.0, right: 0.0),
                    child: Text('Buat',
                        style: TextStyle(
                            fontFamily: 'Nexa Bold',
                            fontWeight: FontWeight.bold,
                            fontSize: 40.0)),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 40.0, left: 50.0, bottom: 39.0, right: 0.0),
                    child: Text('Akun Baru',
                        style: TextStyle(
                          fontFamily: 'Nexa Bold',
                          fontWeight: FontWeight.bold,
                            fontSize: 40.0)),
                  ),
                  
                ],
              ),
            ),
          Container(
              padding: EdgeInsets.only(top: 0.0, left: 50.0, right: 50.0),
              child: Column(
                children: <Widget>[
                  TextField(
                    decoration: InputDecoration(
                        labelText: 'Nama',
                        labelStyle: TextStyle(
                            fontFamily: 'Nexa Light',
                            fontSize: 12,
                            color: Colors.grey),
                        // hintText: 'EMAIL',
                        // hintStyle: ,
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.green))),
                  ),
                  SizedBox(height: 10.0),
                  TextField(
                    decoration: InputDecoration(
                        labelText: 'Nomor Telepon',
                        labelStyle: TextStyle(
                            fontFamily: 'Nexa Light',
                            fontSize: 12,
                            color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.green))),
                    obscureText: true,
                  ),
                  SizedBox(height: 10.0),
                  TextField(
                    decoration: InputDecoration(
                        labelText: 'Email',
                        labelStyle: TextStyle(
                            fontFamily: 'Nexa Light',
                            fontSize: 12,
                            color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.green))),
                  ),
                  SizedBox(height: 10.0),
                  TextField(
                    decoration: InputDecoration(
                        labelText: 'Password',
                        labelStyle: TextStyle(
                            fontFamily: 'Nexa Light',
                            fontSize: 12,
                            color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.green))),
                  ),
                  SizedBox(height: 10.0),
                  TextField(
                    decoration: InputDecoration(
                        labelText: 'Kota',
                        labelStyle: TextStyle(
                            fontFamily: 'Nexa Light',
                            fontSize: 12,
                            color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.green))),
                  ),
                  SizedBox(height: 50.0),
                  Container(
                      height: 45.0,
                      width: 231,
                      child: Material(
                        borderRadius: BorderRadius.circular(20.0),
                        shadowColor: Colors.greenAccent,
                        color: Colors.green,
                        elevation: 7.0,
                        child: GestureDetector(
                          onTap: () {
                Navigator.of(context).pushNamed('/verification');
              },
                          child: Center(
                            child: Text(
                              'Verifikasi Nomor Telepon',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  fontFamily: 'Nexa Bold'),
                            ),
                          ),
                        ),
                      )),
                  
                ],
              )),
          
        ]));
  }
}
