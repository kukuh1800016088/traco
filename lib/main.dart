import 'package:flutter/material.dart';
import 'package:traco/Notification.dart';
import 'package:traco/forgotpw.dart';
import 'package:traco/home.dart';
import 'package:traco/jual.dart';
import 'package:traco/kategori/detail_botol.dart';
import 'package:traco/login.dart';
import 'package:traco/signup.dart';
import 'package:traco/verification.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        '/signup': (BuildContext context) => new SignupPage(),
        '/login': (BuildContext context) => new Login(),
        '/verification': (BuildContext context) => new Verification(),
        '/afterverif': (BuildContext context) => new Login(),
        '/masuk': (BuildContext context) => new Home(),
        '/forgotfinish': (BuildContext context) => new Login(),
        '/forgotpw': (BuildContext context) => new ForgotPw(),
        '/notif': (BuildContext context) => new Notif(),
        '/jual' : (BuildContext context) => new Jual(),
        '/detailbotol' : (BuildContext context) => new DetailBotol(),
        '/logout' : (BuildContext context) => new Login()
      },
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        children: [
          Image.asset("/images/background.jpeg",
          width: double.infinity,
          height: double.infinity,
          fit: BoxFit.cover),
          Align(
            alignment: Alignment.center,
            child: Image(image: AssetImage('/images/logo.png'),
            width: 162,
            ),
          ),
          Positioned(
            bottom: 0,
            right: 0,
            left: 0,
            child: Column(children: [
              GestureDetector(
              onTap: () {
              Navigator.of(context).pushNamed('/login');
              },
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                    color: Colors.white,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 60, right: 60, top: 15, bottom: 15),
                      child: Text('LOGIN', style: TextStyle(color: Colors.black, fontFamily: 'Roboto', fontSize: 13, fontWeight: FontWeight.bold),),
                    ),
                    ),
              ),
              SizedBox(height: 10),
              GestureDetector(
                onTap: () {
                    Navigator.of(context).pushNamed('/signup');
                  },
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.white
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                    color: Colors.black),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 50, right: 50, top: 15, bottom: 15),
                      child: Text('REGISTER', style: TextStyle(color: Colors.white, fontFamily: 'Roboto', fontSize: 13, fontWeight: FontWeight.bold),),
                    ),
                    ),
              ),
              SizedBox(height: 20),
            ],))
        ],), 
    );
  }
}


