import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';

class Beranda extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var cardTextStyle = TextStyle(fontFamily: 'Nexa Bold', fontSize: 16);
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    alignment: Alignment.topCenter,
                    image: AssetImage('/images/bg.png'),
                    fit: BoxFit.fitWidth)),
          ),
          SafeArea(
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Column(
                children: <Widget>[
                  Container(
                    height: 64,
                    margin: EdgeInsets.only(bottom: 55),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child:
                              Image(image: AssetImage('/images/profile.jpg')),
                        ),
                        SizedBox(width: 16),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Hi, Wahyu Nugroho',
                                style: TextStyle(
                                    fontFamily: 'Nexa Bold',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15)),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 75.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              IconButton(
                                  icon: Icon(LineAwesomeIcons.bell),
                                  onPressed: () {
                                    Navigator.of(context).pushNamed('/notif');
                                  }),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: GridView.count(
                        mainAxisSpacing: 10,
                        crossAxisSpacing: 10,
                        primary: false,
                        children: <Widget>[
                          Card(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Image.asset(
                                  '/images/tongsampah.png',
                                  height: 72,
                                )
                              ],
                            ),
                          )
                        ],
                        crossAxisCount: 1,
                        ),
                  ),
                  SizedBox(height: 20),
                  Text('Kategori', style: TextStyle(
                            fontFamily: 'Nexa Bold', fontSize: 16
                          ),),
                          SizedBox(height: 20),
                   Expanded(
                    child: GridView.count(
                        mainAxisSpacing: 10,
                        crossAxisSpacing: 10,
                        primary: false,
                        children: <Widget>[
                          
                          GestureDetector(
                            onTap: () {
                            Navigator.of(context).pushNamed('/detailbotol');
                          },
                            child: Card( 
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                              elevation: 4,
                              child:  
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  
                                  Image.asset(
                                    '/images/botol.jpg',
                                    height: 72,
                                  ),
                                  Text('Botol Plastik', style: cardTextStyle)
                                ],
                              ),
                            ),
                          ),
                           Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  '/images/kertas.jpg',
                                  height: 72,
                                ),
                                SizedBox(height: 10),
                                Text('Kertas', style: cardTextStyle)
                              ],
                              
                            ),
                          ),

                          Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  '/images/kaleng.jpg',
                                  height: 72,
                                ),
                                Text('Logam', style: cardTextStyle)
                              ],
                            ),
                          ),

                          Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  '/images/elektronik.jpg',
                                  height: 72,
                                ),
                                Text('Elektronik', style: cardTextStyle)
                              ],
                            ),
                          ),

                          Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  '/images/karet.jpg',
                                  height: 72,
                                ),
                                Text('Karet', style: cardTextStyle)
                              ],
                            ),
                          ),

                          Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  '/images/kaca.jpg',
                                  height: 72,
                                ),
                                Text('Kaca', style: cardTextStyle)
                              ],
                            ),
                          )
                        ],
                        crossAxisCount: 3,
                        ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
