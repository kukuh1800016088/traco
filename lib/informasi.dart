import 'package:flutter/material.dart';

class Informasi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var cardTextStyle = TextStyle(fontFamily: 'Nexa Bold', fontSize: 16);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          elevation: 0,
          title: Text(
            'Informasi',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontFamily: 'Nexa Bold',
              fontSize: 18,
            ),
          ),
        ),
        body: SafeArea(
          child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Column(
                children: <Widget>[
                  
                  Expanded(
                    child: GridView.count(
                        mainAxisSpacing: 10,
                        crossAxisSpacing: 10,
                        primary: false,
                        children: <Widget>[
                          Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  '/images/berita.png',
                                  fit: BoxFit.fitWidth,
                                ),
                                Text('Mari pilah sampahmu, selamatkan bumimu', style: cardTextStyle)
                              ],
                            ),
                          ),
                           Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  '/images/berita.png',
                                  fit: BoxFit.fitWidth,
                                ),
                                SizedBox(height: 10),
                                Text('Mari pilah sampahmu, selamatkan bumimu', style: cardTextStyle)
                              ],
                              
                            ),
                          ),

                          Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Image.asset(
                                  '/images/berita.png',
                                  fit: BoxFit.fitWidth,
                                ),
                                Text('Mari pilah sampahmu, selamatkan bumimu', style: cardTextStyle)
                              ],
                            ),
                          ),

                          
                        ],
                        crossAxisCount: 1,
                        ),
                  ),
                ],),
    ),
    )
      ),
    );
  }
}